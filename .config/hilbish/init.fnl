#!/usr/bin/env fennel

;; @TODO(Renzix): move this (and other macros) to macro.fnl
;; MACROS
(macro module! [...]
  (var ret {})
  (for [i 1 (length [...])]
    (if (list? (. [...] i))
        (table.insert ret `(local ,(. (. [...] i) 1) (require ,(. (. [...] i) 2))))
        (table.insert ret `(require ,(. [...] i)))))
  ret)

;; SETUP
(macro alias! [name ...]
  (var ret nil)
  (for [i 1 (length [...]) :until (not= ret nil)]
    (if (not= (hilbish.which (. [...] i)) "")
          (set ret `(hilbish.alias ,(tostring name) ,(. [...] i)))))
  ret)

(macro hook! [hook func]
  `(b.catch ,(tostring hook) ,func))

(macro defadvice! [command func]
  `(com.register ,command ,func))

(macro add-path! [...]
  (var ret {})
  (each [_ value (ipairs [...])]
    (table.insert ret `(hilbish.appendPath ,value)))
  ret)

(macro env! [name value]
  (if (not= value nil)
  `(os.setenv ,name ,value)
  `(os.getenv ,name)))

(macro sh! [str]
  `(hilbish.run ,str))

;; ACTUAL CONFIG
;; MODULES
(module!  (b :bait)
          (com :commander)
          (f :fennel)
          (syntax :syntax-highlighting)
          (colors :lunacolors))

;; Aliases
(alias! ls "ls --color=auto")
(alias! ht "htop")
(alias! g "grep -rn")
(alias! e "emacs -nw -Q" "nvim" "vim" "vi")

;; Prompt
(fn left-prompt [fail]
    (hilbish.prompt (colors.format
                      (.. (if (= fail  0) "{green}" "{red}") "%D "))))

(fn right-prompt [fail]
    (hilbish.prompt (os.date  "!%a %b %d, %H:%M" (+ (os.time) (* 8  60  60 ))) "right" ))

(left-prompt 0)
(right-prompt 0)
(hook! command.exit left-prompt)
(hook! command.exit right-prompt)

;; environment vars
(add-path! "/opt/plan9/bin" "/.config/emacs/bin" "~/.local/bin")
(env! "GOPATH" (.. (env! "HOME") "/.go"))

;; Use fennel to run shit
;; (hilbish.runnerMode (lambda [input]
                      ;; (if (pcall fennel.eval input {"useMetadata" true})
                          ;; (values [0 nil])
                          ;; (hilbish.runner.sh input))))

(fn hilbish.highlighter [line]
  (syntax.sh line))
;; (syntax.sh "testing")

;; (print (syntax.sh ""))
;; (print (syntax.sh "test this is \"some text\""))
;; (print (syntax.sh "tst thir si more"))


;;(defadvice! "cd" #(sh! "ls"))
